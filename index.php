<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Upload</title> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
body {
    margin:130px; margin-left: 220px; background-image: url(bag.jpg); background-color: #fff;
}
a:link, a:visited {
    color: blue; text-decoration: none;
    }
a:hover, a:active {
    color: red;  text-decoration: none;
}
div#page {
    width:512px; height: 261px;
}
div#top {
    width:512px; height: 26px; background-image: url(1.png);
}
div#cent {
    width:512px; height: 210px; background-image: url(2.png);
}
div#bott {
    width:512px; height: 25px; background-image: url(3.png);
}
ol, p#instr {
    font-size: 12px;
}
p#textop {
    padding-left:45px; padding-top: 3px;
}
div#bad {
    color:red; font-size: 14px; padding:3px 0px 2px 7px;
}
div#ok {
    color:green; font-size: 14px; padding:3px 0px 2px 7px;
}
div#copy {
    font-size: 10px; text-align: center;
}
input.put {
    display: block;
}
div#formo {
    padding: 10px; padding-left:25px; font-size: 12px;
}
span#copy {
    font-size: 10px; padding-left:200px;
}
</style>
</head> 
<body>
<div id="page"> 
<div id="top"> <p id="textop">Podajnik v1.4 beta</p></div>
<div id="cent"><div id="formo">
<form enctype="multipart/form-data" action="index.php" method="post"> <p>
<?php
error_reporting(E_ALL | E_NOTICE);
$typy_plikow = array('.jpg','.gif','.bmp','.png','.pdf','.zip','.7z','.rar','.doc','.docx');
$max_filesize = 1024*1024*10; // 10MB
$upload_path = 'files/'; 
?>
<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_filesize; ?>" />
<input type="hidden" name="ok" value="1" />
<input class="put" name="plik" type="file" />
<?php

$iNumberOne = rand(1,10);
$iNumberTwo = rand(1,10);
$wynik = $iNumberOne + $iNumberTwo;
echo $iNumberOne.' + '.$iNumberTwo;
?>
 = <input type="text" name="wynik" size="3" />
 <input type="hidden" name="suma" value="<?php echo $wynik; ?>" />
Wpisz hasło: <input type="password" name="pass" size="9" /><br />
<input type="submit" value="Wyślij plik" /></p></form>
<p id="instr">Krótka instrukcja obsługi:</p>
<ol>
<li>Wybierz plik do wysłania, dozwolone formaty to: ".jpg",".gif",".bmp",".png",".pdf",".zip", ".rar", ".7z" '.doc' i'.docx'</li>
<li>Wpisz hasło i wynik działania matematycznego.</li>
<li>Po naciśnięciu przycisku "Wyślij plik" proszę spokojnie czekać na wyświetlenie komunikatu.</li>
</ol>
</div>
</div>
<div id="bott">
<?php

if (isset($_POST) && isset($_FILES)) {
  if (isset($_POST['pass']) && isset($_FILES['plik'])) {
    $pass = $_POST['pass'];
    $plik_nazwa = $_FILES['plik']['name'];
    $plik_error = $_FILES['plik']['error'];
    $plik_tmp = $_FILES['plik']['tmp_name'];
    $ext = substr($plik_nazwa, strpos($plik_nazwa,'.'), strlen($plik_nazwa)-1);
    $k = $_POST['ok'];
    $next = '</div></div><p><span id="copy">Caraxes &copy; 2008/2011</span></p></body></html>';

   if($k == 1){
	if(!empty($_POST['wynik']) OR !empty($_POST['suma'])) {
		if((int)$_POST['wynik'] != (int)$_POST['suma']) die('<div id="bad">Podano błędny wynik.</div> '.$next);
	} else die('<div id="bad">Podano błędny wynik.</div> '.$next);
	

   if($pass !== 'spacja')
        die('<div id="bad">Błędne hasło.</div> '.$next);

   //if(!in_array(strtolower($ext),$typy_plikow))
   //   die('<div id="bad">Błędny typ pliku.</div> '.$next);
 
   if(filesize($plik_tmp) > $max_filesize)
      die('<div id="bad">Plik jest za duzy (>10MB)</div> '.$next);
      
   if(file_exists($upload_path.$plik_nazwa))
      die('<div id="bad">Plik o podanej nazwie istnieje.</div> '.$next);
      
   if(move_uploaded_file($plik_tmp,$upload_path.$plik_nazwa))
         echo '<div id="ok">Upload zakończony sukcesem. Plik został przesłany <a href="'.$upload_path.$plik_nazwa.'" title="plik">tutaj</a></div> ';
   else
         echo '<div id="bad">Podczas przesyłania pliku '.$plik_nazwa.' wystąpił błąd. Proszę spróbować ponownie.</div>'; 

     }
   }
}   
echo '</div></div><p><span id="copy">Caraxes &copy; 2008/2011</span></p></body></html>';
?>
